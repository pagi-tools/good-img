#!/bin/bash

PROJECT_URL=$PROJECT_URL
REG_TOKEN=$REG_TOKEN
cd /runner

sudo service docker start
./config.sh --url ${PROJECT_URL} --token ${REG_TOKEN} --disableupdate

cleanup() {
    echo "Removing runner..."
    ./config.sh remove --unattended --token ${REG_TOKEN}
}

trap 'cleanup; exit 130' INT
trap 'cleanup; exit 143' TERM

./run.sh & wait $!
